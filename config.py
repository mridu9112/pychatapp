import os
basedir = os.path.abspath(os.path.dirname(__file__))

#basic configuration that triggers everytime..
class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'socketkey'
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:admin@localhost/chat_app'


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True

#currently using for development
class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True