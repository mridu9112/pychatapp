from flask import Flask, render_template, jsonify
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_sqlalchemy import SQLAlchemy
import os, config
#import database as dbs

from datetime import datetime

app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
socketio = SocketIO(app)
db = SQLAlchemy(app)

import database

@app.route('/')
def index():
    return render_template('chat.html')

@socketio.on('user-connected')
def handle_user_conn(json):
    print('received message:'+str(json)) 
    socketio.emit('server-response', json)

@socketio.on('message-posted')
def handle_message(data):
    data1= dict(data)
    chat = database.chats('id', sender=data[u'senid'], receiver=data[u'rid'] , message=data[u'msg'], time=str(datetime.now()) )
    db.session.add(chat)
    db.session.commit()
    print('message:'+str(data)) 

    
    messages = db.session.query(database.chats).filter(database.chats.sender.in_((data[u'senid'],data[u'rid'])),database.chats.receiver.in_((data[u'senid'],data[u'rid']))).all()
    c = []
    p = {}
    for i in xrange(len(messages)):
        p['chatid']=messages[i].chatid
        p['receiver']=messages[i].receiver
        p['sender']=messages[i].sender
        p['message']=messages[i].message
        p['time']=str(messages[i].time)
        c.append(p)
        p = {}
    
    socketio.emit('server-response-msg', c)

if __name__=='__main__':
    socketio.run(app, debug=True, port=8000)

