from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from server import db
import setting

#model for storing user login data..
class users(db.Model):
    __tablename__ = 'user'

    userid = db.Column(db.String(20), primary_key=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(80))
    
    def __init__(self, userid, email, password):
        self.userid = userid
        self.email = email
        self.password = password

    def __repr__(self):
        return '< userid: {}, email: {}, password: {} >'.format(self.userid, self.email, self.password)

#model for storing chats
class chats(db.Model):
    __tablename__ = 'chat'

    chatid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    sender = db.Column(db.String(120))
    receiver = db.Column(db.String(120))
    message = db.Column(db.String(500))
    time = db.Column(db.DateTime)
    
    def __init__(self, chatid, sender, receiver, message, time):
        self.sender = sender
        self.receiver = receiver
        self.message = message
        self.time = time

    def __repr__(self):
        return '< chatid :{}, sender:{}, receiver:{}, message:{}, time:{} >'.format(self.chatid, str(self.sender), str(self.receiver), str(self.message), self.time)
