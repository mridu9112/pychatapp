from datetime import timedelta
import os

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
BCRYPT_LOG_ROUNDS = 13

ASSETS_DEBUG = False
DEBUG_TB_ENABLED = False  # Disable Debug toolbar
DEBUG_TB_INTERCEPT_REDIRECTS = False
LOG_RESPONSE = True

CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
WTF_CSRF_ENABLED = False

SQLALCHEMY_COMMIT_ON_TEARDOWN = True
ADMINS = ['mridul.naithani@gmail.com']
MANAGERS = ADMINS

DEBUG = False
TESTING = True